import React from "react";
import { ThemeProvider, createGlobalStyle, css } from "styled-components";

const makeMediaQuery = query => (...args) => css`
  @media ${query} {
    ${css(...args)}
  }
`;

const AwaymoTheme = {
  colors: {
    accent: "#ee5f63",
    text: "#fff"
  },
  atmedia: {
    large: makeMediaQuery(`(min-width: 1024px)`),
    mid: makeMediaQuery(`(min-width: 769px) and (max-width: 1023px)`),
    small: makeMediaQuery(`(max-width: 768px)`)
  }
};

const GlobalStyle = createGlobalStyle`
  html, body {
    font-family: Lato, sans-serif;
    font-weight:300;
    width: 100%;
    height: 100%;
    margin:0;
    padding:0;
  }
  body {
    ${({ theme }) => theme.atmedia.large`
      font-size:28px;
    `}
    font-size:20px;
  }
  strong, h1, h2, h3 {
    font-weight:900;
  }
  #root {
    min-height: 100%;
  }
`;

export default ({ children }) => (
  <ThemeProvider theme={AwaymoTheme}>
    <>
      <GlobalStyle />
      {children}
    </>
  </ThemeProvider>
);
