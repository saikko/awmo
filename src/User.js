import React, { useReducer, useContext } from "react";

export const mockUser = {
    displayName: "Pawel Knapik",
    accountBalance: {
        currency: "GBP",
        amount:1000
    }
}

export const UserContext = React.createContext({});


export const UserProvider =  ({reducer, initialState, children}) =>(
  <UserContext.Provider value={useReducer(reducer, initialState)}>
    {children}
    </UserContext.Provider>);

export const useContextUser = () => useContext(UserContext);


