import React from "react";
import styled from "styled-components";
import { Router, Route } from "react-router-dom";
import { createBrowserHistory } from "history";
import ThemeProvider from "./Theme";
import { UserProvider, mockUser } from "./User";
import AppBackground from "./AppBackground";
import SiteNavigation from "./components/SiteNavigation";

const history = createBrowserHistory();

const Pathname = styled.div`
  height: 80vh;
  width: 960px;
  max-width: 100%;
  margin: 10vh auto;
  color: ${props => props.theme.colors.text};
  text-align: center;
  font-size: 2rem;
`;

const App = () => {
  const initialState = {
    loggedIn: false,
    displayName: null
  };
  const reducer = (state, action) => {
    switch (action.type) {
      case "logIn":
        return {
          ...state,
          ...mockUser,
          loggedIn: true
        };
      case "logOut":
        return {
          ...state,
          ...initialState
        };
      default:
        return state;
    }
  };
  return (
    <ThemeProvider>
      <UserProvider initialState={initialState} reducer={reducer}>
        <Router history={history}>
          <AppBackground>
            <SiteNavigation history={history} />

            <Route
              component={({ location }) => (
                <Pathname>awaymo.com{location.pathname}</Pathname>
              )}
            />
          </AppBackground>
        </Router>
      </UserProvider>
    </ThemeProvider>
  );
};

export default App;
