import styled from "styled-components";

const AppBackground = styled.div`
  background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0)),
    url(/img/introBackgroundSummer.jpg);
  background-size: auto, cover;
  background-attachment: fixed;
  background-position:50% 50%;
  width: 100%;
  min-height: 100vh;
`;


export default AppBackground;
