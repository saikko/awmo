import React from "react";
import styled from "styled-components";
import { useContextUser } from "../User";
import AccountBalance from "./AccountBalance";
import Avatar from "./Avatar";
import { Img, Link } from "./styled";
import I from "./Icons";

const ProfileBoxWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 2rem;
  padding-bottom: 1rem;

  ${Avatar} {
    margin-top: 0.5rem;
    margin-bottom: 0.5rem;
  }

  ${({ theme }) => theme.atmedia.large`
    border-bottom:1px solid rgba(255,255,255,0.5);
    flex-direction: row;
    > * + * {
      margin-left: 1rem;
    }
  `}
`;

const ProfileBox = ({ displayName, avatar = "/avatar.png", ...props }) => (
  <ProfileBoxWrapper>
    <Avatar src={avatar} />
    <div>
      <strong>{displayName}</strong>
      <AccountBalance {...props} />
    </div>
  </ProfileBoxWrapper>
);

const UserBox = styled(({ className, ...props }) => {
  const [userData, dispatch] = useContextUser();
  const loginHandler = e => {
    e.preventDefault();
    dispatch({ type: "logIn" });
  };

  return (
    <div className={className}>
      {userData.loggedIn ? (
        <ProfileBox {...userData} />
      ) : (
        <>
          <Link subtext href="/registration" onClick={loginHandler}>
            <I icon="user-plus" />
            Create Account
            <small>
              Create account, book a flight, have a good time, pay in small
              parts later
            </small>
          </Link>
          <Link subtext href="/login" onClick={loginHandler}>
            <I icon="sign-in-alt" />
            Log In
            <small>
              Book more, travel more, still pay in small parts later
            </small>
          </Link>
        </>
      )}
      {props.children}
    </div>
  );
})``;

export default UserBox;
