import styled from "styled-components";

const UL = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0.5rem 0;
  li {
    margin: 0;
    padding: 0;
  }
`;

export default UL;
