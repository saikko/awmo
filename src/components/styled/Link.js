import React from "react";
import styled, { css } from "styled-components";
import { Link as RouterLink } from "react-router-dom";

const Link = styled(props => {
  let { to, href, external, ...rest } = props;
  const linkTo = to || href;

  return external ? (
    <a href={linkTo} target="_blank" {...rest} />
  ) : (
    <RouterLink to={linkTo} {...rest} />
  );
})`
  text-decoration: none;
  color: ${props => props.theme.colors.text};
  &::after {
    content: "";
    display: block;
    height: 1px;
    border-top: 1px solid rgba(255, 255, 255, 0.5);

    ${({ theme }) => theme.atmedia.large`
      opacity: 0;
      transition: transform 300ms, opacity 200ms;
      transform: scaleX(0);
      transform-origin: 0 0;
    `}
  }
  &:hover::after {
    opacity: 1;
    transform: scale(1);
  }

  ${props =>
    props.subtext &&
    css`
      small {
        display: block;
        font-size: 0.75em;
        font-weight: 300;
        ${({ theme }) => theme.atmedia.small`
      display:none;
      `}
      }
    `}
`;

export default Link;
