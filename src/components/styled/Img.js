import styled from "styled-components";

const Img = styled.img`
border:none;
display:block;
margin:0;
max-width:100%;
`;

export default Img;
