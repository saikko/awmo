import React from "react";
import styled from "styled-components";
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import {
  faCreditCard,
  faEnvelope,
  faFile,
  faInfoCircle,
  faLifeRing,
  faPhone,
  faPlane,
  faUserPlus,
  faQuestionCircle,
  faSignInAlt,
  faSignOutAlt,
  faUserCircle
} from "@fortawesome/free-solid-svg-icons";

[
  faCreditCard,
  faEnvelope,
  faFile,
  faInfoCircle,
  faLifeRing,
  faPhone,
  faPlane,
  faUserPlus,
  faQuestionCircle,
  faSignInAlt,
  faSignOutAlt,
  faUserCircle
].forEach(i => library.add(i));

const Icon = styled(props => <FontAwesomeIcon {...props} />)`
  margin-right: 0.5em;
  ${({ theme }) => theme.atmedia.large`
    display:none;
  `}
`;

export default Icon;
