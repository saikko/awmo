import React from "react";
import styled from "styled-components";
import { withRouter } from "react-router";
import { CSSTransition } from "react-transition-group";

import Logo from "./Logo";
import NavOverlay from "./NavOverlay";
import Nav from "./Nav";

const Wrapper = styled.div`
  background-color: ${props => props.theme.colors.accent};
  color: ${props => props.theme.colors.text};

  display: grid;
  grid-template-rows: 1fr auto;
  grid-template-columns: 100%;
  min-height: 100vh;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  z-index: 1;
  opacity: 0;
  transform: translateX(-100%);
  ${({ theme }) => theme.atmedia.large`
    transform: translateY(-100%);
  `}
  &.nav-enter {
    opacity: 0;
    transform: translateX(-100%);
    ${({ theme }) => theme.atmedia.large`
      transform: translateY(-100%);
    `}
  }
  &.nav-enter-active {
    opacity: 1;
    transform: translateX(0);
    transition: opacity 300ms, transform 400ms ease-in-out;
    ${({ theme }) => theme.atmedia.large`
       transform: translateY(0);
    `}
  }
  &.nav-enter-done {
    opacity: 1;
    transform: translateX(0);
    ${({ theme }) => theme.atmedia.large`
       transform: translateY(0);
    `}
  }
  &.nav-exit {
    opacity: 1;
    transform: translateX(0);
    ${({ theme }) => theme.atmedia.large`
       transform: translateY(0);
    `}
  }
  &.nav-exit-active {
    opacity: 0;
    transform: translateX(-100%);
    transition: opacity 200ms, transform 200ms ease-in-out;
    ${({ theme }) => theme.atmedia.large`
       transform: translateY(-100%);
    `}
  }
  &.nav-exit-done {
    opacity: 0;
    transform: translateX(-100%);
    ${({ theme }) => theme.atmedia.large`
       transform: translateY(-100%);
    `}
  }
`;

const Header = styled.header`
  padding: 1rem 0;
  margin: 0 1rem;
  ${Logo} {
    position: relative;
    margin-left: 0;
    margin-bottom: 2rem;
    z-index: 6;
  }
`;
const Help = styled.aside`
  text-align: center;
  padding: 1em 0;
  margin: 0 1rem;
  font-size: 1rem;
`;

const NavigationLayout = ({ isOpen, toggleOpen, ...props }) => {
  return (
    <Header>
      <Logo
        accent={isOpen ? "#FFF" : "#ee5f63"}
        color="#fff"
        width={180}
        height={40}
      />
      <CSSTransition in={isOpen} timeout={300} classNames="nav">
        <Wrapper>
          <Nav />
          <Help>
            We're here to help +44 (0) 20 8050 3459 support@awaymo.com
          </Help>
        </Wrapper>
      </CSSTransition>
    </Header>
  );
};

const SiteNavigation = ({ history, ...props }) => {
  return (
    <NavOverlay history={history}>
      <NavigationLayout {...props} />
    </NavOverlay>
  );
};

export default SiteNavigation;
