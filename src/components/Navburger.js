import React from "react";
import styled from "styled-components";

const BurgerWrapper = styled.div`
  user-select: none;
  padding-right: 0.5rem;
  cursor: pointer;
  rect {
    transition: transform 200ms ease-in-out;
  }
`;
const Burger = ({ isOpen, ...props }) => (
  <BurgerWrapper {...props}>
    <svg width="24" height="24" viewBox="-4 -4 34 34">
      <rect
        x="0"
        y="0"
        height="4"
        width="40"
        fill="#fff"
        style={{
          transform: isOpen ? `rotate(45deg)` : `rotate(0)`,
          transformOrigin: "2px 2px"
        }}
      />
      <rect
        x="0"
        y="13"
        height="4"
        width="40"
        fill="#fff"
        opacity={isOpen ? 0 : 1}
      />
      <rect
        x="0"
        y="26"
        height="4"
        width="40"
        fill="#fff"
        style={{
          transform: isOpen ? `rotate(-45deg)` : `rotate(0)`,
          transformOrigin: "2px 28px"
        }}
      />
    </svg>
  </BurgerWrapper>
);

export default Burger;
