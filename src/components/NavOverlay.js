import React, { useState, useEffect } from "react";
import styled from "styled-components";
import Navburger from "./Navburger";

const Burger = styled(Navburger)`
  position: fixed;
  right: 1rem;
  top: 1rem;
  z-index: 6;
`;

const Overlay = ({ children = null, history, ...props }) => {
  const [isOpen, toggleOpen] = useState(false);
  useEffect(() => {
    return history.listen((location, action) => {
      toggleOpen(false);
      console.log(action, location.pathname, location.state);
    });
  }, []);

  return (
    <>
      <Burger isOpen={isOpen} onClick={e => toggleOpen(!isOpen)} />
      {React.cloneElement(children, { isOpen, toggleOpen, ...props })}
    </>
  );
};

export default Overlay;
