import styled from "styled-components";
import { Img } from "./styled";

const Avatar = styled(Img)`
  width: 3rem;
  height: 3rem;
  border-radius: 100%;
  background: #fff;
  border: 1px solid ${props => props.theme.colors.accent};
  box-shadow: 0 0 0 4px #fff;
  padding: 4px;
`;

export default Avatar;
