import React from "react";
import styled from "styled-components";

const accountBalanceFormat = ({ amount = 0, currency = "GBP" }) =>
  amount.toLocaleString(undefined, {
    style: "currency",
    currency
  });

const AccountBalance = ({ accountBalance }) => (
  <div>Account Balance: <strong>{accountBalanceFormat(accountBalance)}</strong></div>
);


export default AccountBalance;
