import React from "react";
import styled from "styled-components";
import { useContextUser } from "../User";
import ProfileBox from "./ProfileBox";
import { Link, UL } from "./styled";
import I from "./Icons";

const PageNav = styled(props => (
  <UL {...props}>
    <li>
      <Link href="/">Home</Link>
    </li>
    <li>
      <Link href="/flights">Flights</Link>
    </li>
  </UL>
))`
  ${({ theme }) => theme.atmedia.large`
   grid-column: 1;
`}
`;
const CompanyNav = styled(props => (
  <UL {...props}>
    <li>
      <Link href="/about">
        <I icon="question-circle" />
        About us
      </Link>
    </li>
    <li>
      <Link external href="https://intercom.help/awaymo">
        <I icon="info-circle" />
        FAQ
      </Link>
    </li>
    <li>
      <Link external href="https://intercom.help/awaymo">
        <I icon="phone" />
        Support
      </Link>
    </li>
    <li>
      <Link href="/contacts">
        <I icon="envelope" />
        Contact us
      </Link>
    </li>
  </UL>
))`
  ${({ theme }) => theme.atmedia.large`
   grid-column: 1;
`}
`;
const PersonalNav = styled(({ logOut, ...props }) => (
  <UL {...props}>
    <li>
      <Link href="/user">
        <I icon="user-circle" />
        Profile
      </Link>
    </li>
    <li>
      <Link href="/user/bookings">
        <I icon="plane" />
        My Bookings
      </Link>
    </li>
    <li>
      <Link href="/user/payments">
        <I icon="credit-card" />
        My Payments
      </Link>
    </li>
    <li>
      <Link href="" onClick={logOut}>
        <I icon="sign-out-alt" />
        Log Out
      </Link>
    </li>
    <li>
      <Link href="">
        <I icon="file" />
        Resume Application
      </Link>
    </li>
  </UL>
))`
  ${({ theme }) => theme.atmedia.large`
   grid-column: 2;
`}
`;

const Nav = styled.nav`
  border-top: 1px solid #fff;
  border-bottom: 1px solid #fff;
  margin: 80px auto 0;
  width: 960px;
  max-width: 100%;
  display:flex;
  flex-direction:column;

  > * { 
    width: 360px; 
    max-width: 100%; 
    margin-left:auto;
    margin-right:auto;
  }

  ${ProfileBox} {
    order:1;
  }
  ${PageNav}{
    display:none;
  }
  ${CompanyNav}{
    order:2;
  }

  ${({ theme }) => theme.atmedia.large`
    display: grid;
    grid-template-columns: 1fr 3fr;
    align-content:start;
    padding:4rem 0;
    
    > * { 
        width:100%;
    }
    ${ProfileBox} {
      grid-row:1 / span 2;
      grid-column:2;
    }
    ${PageNav}{
      display:block;
      grid-column:1;
      grid-row:1;
    }
    ${CompanyNav}{
      grid-column:1;
      grid-row:2;
    }
  `}
  ${Link} {
    font-weight: 900;
    display:block;
    padding:0.75rem;
    &::after {
      position:relative; 
      top:0.75rem;
    }
    ${({ theme }) => theme.atmedia.large`
      padding:0;
      display:inline-block;
      &::after { 
        top:0;
          border-top-width:2px;
      }
    `}
`;

const Navigation = props => {
  const [userData, dispatch] = useContextUser();
  const logOut = e => dispatch({ type: "logOut" });
  return (
    <Nav>
      <PageNav />
      <CompanyNav />
      <ProfileBox>
        {userData.loggedIn && <PersonalNav logOut={logOut} />}
      </ProfileBox>
    </Nav>
  );
};

export default Navigation;
