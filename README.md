# React technical test

The object of this test is to showcase my coding abilities according to specs and industry standards.

## Highlights

### fully responsive: ✓
Media queries with breakpoints are defined in the theme and used throughout the code like this:
```
const ResponsiveDiv = styled.div`
  background:tomato;
  ${ props => props.theme.atmedia.small`
    background:orange;
  `}
  ${ props => props.theme.atmedia.large`
    background:pink;
  `}
`
```
Usually the mobile styling is defined with no media query at all, then overridden with desktop-specific details using `atmedia`. 

### animated: ✓

Slide-in/out + fade-in/out effect when showing/hiding the navigation; hides a bit quicker than than shows, to get out of the way asap. Slides horizontally on devices, vertically on desktop.
Additional animations include 
- the menu button icon (animates from "hamburger" shape into "X" and back). 
- the accent color in company logo fades from pink to white and back (logo was converted to an inline SVG element with transitions)
- hover effects on navigation items

### looks good on desktops and devices: ✓
At least on the ones I own ;)
For better experience I took the liberty to use the background image from company website.
Also keeping the same logo and toggle button components on screen resolved the problem of these elements being misaligned between open and closed state (as is the case on the current website).

### `styled-components`: ✓
The basic HTML building blocks are in `src/components/styled`, then used to build page-specific components. I did my best to keep it as DRY as possible.

### `react-transition-group`: ✓
I haven't used the library before and my first choice would probably be to define the transitions with `styled-components` CSS, but it works good and doesn't get in the way, so I have decided to go with it.

### fontawesome: ✓
It's a bit verbose (`import`, then `library.add`, then `icon="different-case"`), but well, here it is.

## Technical details

### State management
I didn't want to introduce a state management library and decided to stick to hooks. The "Log in" and "Create account" buttons set the user state to `loggedIn` and make user details available to the user profile box, then "Log out" switches back to signed out state.
 
The navigation open/closed state is internal to the component, implemented with hooks as well.

### Routing
It is outside of the scope of this task, but I have added `react-router` to better show the navigation behavior when navigating between routes (so it hides when user clicks on an internal link).

Ideally the navigation items would be rendered by mapping entries in some kind of `routes` object that defines the URLs, icons and labels, but this would be premature at this stage without specifying it first.

### Trivia
The Accound Balance value is using `Number.toLocaleString` using the default formatting for user locale, and currency defined in their "wallet".

External links (FAQ & Suppoer) open in new tabs to not mess with the router.

## Building, running, viewing
as default in a `create-react-app` based project.
for a quick look please visit https://saikko.pl/awmo/